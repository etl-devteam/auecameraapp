/*
 * camera.cpp
 *
 *  Created on: 26 giu 2019
 *      Author: andre
 */

#include <sys/stat.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <linux/videodev2.h>
#include <errno.h>
#include <unistd.h>
#include <cmath>
#include <string.h>
#include <fcntl.h>
#include <dirent.h>

#include "ALog.h"
#include "Camera.h"


#define CLIP(color) (unsigned char)(((color) > 0xFF) ? 0xff : (((color) < 0) ? 0 : (color)))


static int xioctl(int fh, int request, void *arg)
{
    int r;

    do {
        r = ioctl(fh, request, arg);
    } while (-1 == r && EINTR == errno);

    return r;
}

namespace VisionAR {

    static const int DEBUG = true;
    static const char *TAG = "CAMERA";

    typedef struct {
        unsigned char r;
        unsigned char g;
        unsigned char b;
        unsigned char a;
    } RGB32;

    static const std::string VIDEO_DIR = "/dev";
    static const std::string VIDEO_FILE = "video";
    static const std::string VIDEO_BUS = "usb";
    static const std::string CTRL_TYPE[] = { "NO TYPE",
                                             "V4L2_CTRL_TYPE_INTEGER",
                                             "V4L2_CTRL_TYPE_BOOLEAN",
                                             "V4L2_CTRL_TYPE_MENU",
                                             "V4L2_CTRL_TYPE_BUTTON",
                                             "V4L2_CTRL_TYPE_INTEGER64",
                                             "V4L2_CTRL_TYPE_CTRL_CLASS",
                                             "V4L2_CTRL_TYPE_STRING",
                                             "V4L2_CTRL_TYPE_BITMASK",
                                             "V4L2_CTRL_TYPE_INTEGER_MENU" };
    static const std::string USERCTRL_ID[] = {"V4L2_CID_BRIGHTNESS",
                                              "V4L2_CID_CONTRAST",
                                              "V4L2_CID_SATURATION",
                                              "V4L2_CID_HUE",
                                              "",
                                              "V4L2_CID_AUDIO_VOLUME",
                                              "V4L2_CID_AUDIO_BALANCE",
                                              "V4L2_CID_AUDIO_BASS",
                                              "V4L2_CID_AUDIO_TREBLE",
                                              "V4L2_CID_AUDIO_MUTE",
                                              "V4L2_CID_AUDIO_LOUDNESS",
                                              "V4L2_CID_BLACK_LEVEL",
                                              "V4L2_CID_AUTO_WHITE_BALANCE",
                                              "V4L2_CID_DO_WHITE_BALANCE",
                                              "V4L2_CID_RED_BALANCE",
                                              "V4L2_CID_BLUE_BALANCE",
                                              "V4L2_CID_WHITENESS",
                                              "V4L2_CID_EXPOSURE",
                                              "V4L2_CID_AUTOGAIN",
                                              "V4L2_CID_GAIN",
                                              "V4L2_CID_HFLIP",
                                              "V4L2_CID_VFLIP",
                                              "",
                                              "",
                                              "V4L2_CID_POWER_LINE_FREQUENCY",
                                              "V4L2_CID_HUE_AUTO",
                                              "V4L2_CID_WHITE_BALANCE_TEMPERATURE",
                                              "V4L2_CID_SHARPNESS",
                                              "V4L2_CID_BACKLIGHT_COMPENSATION",
                                              "V4L2_CID_CHROMA_AGC",
                                              "V4L2_CID_COLOR_KILLER",
                                              "V4L2_CID_COLORFX" };
    static const std::string CAMERACTRL_ID[] = {"",
                                                "V4L2_CID_EXPOSURE_AUTO",
                                                "V4L2_CID_EXPOSURE_ABSOLUTE",
                                                "V4L2_CID_EXPOSURE_AUTO_PRIORITY",
                                                "V4L2_CID_PAN_RELATIVE",
                                                "V4L2_CID_TILT_RELATIVE",
                                                "V4L2_CID_PAN_RESET",
                                                "V4L2_CID_TILT_RESET",
                                                "V4L2_CID_PAN_ABSOLUTE",
                                                "V4L2_CID_TILT_ABSOLUTE",
                                                "V4L2_CID_FOCUS_ABSOLUTE",
                                                "V4L2_CID_FOCUS_RELATIVE",
                                                "V4L2_CID_FOCUS_AUTO",
                                                "V4L2_CID_ZOOM_ABSOLUTE",
                                                "V4L2_CID_ZOOM_RELATIVE",
                                                "V4L2_CID_ZOOM_CONTINUOUS",
                                                "V4L2_CID_PRIVACY",
                                                "V4L2_CID_IRIS_ABSOLUTE",
                                                "V4L2_CID_IRIS_RELATIVE",
                                                "V4L2_CID_AUTO_EXPOSURE_BIAS",
                                                "V4L2_CID_AUTO_N_PRESET_WHITE_BALANCE",
                                                "V4L2_CID_WIDE_DYNAMIC_RANGE",
                                                "V4L2_CID_IMAGE_STABILIZATION",
                                                "V4L2_CID_ISO_SENSITIVITY",
                                                "V4L2_CID_ISO_SENSITIVITY_AUTO",
                                                "V4L2_CID_EXPOSURE_METERING",
                                                "V4L2_CID_SCENE_MODE",
                                                "V4L2_CID_3A_LOCK",
                                                "V4L2_CID_AUTO_FOCUS_START",
                                                "V4L2_CID_AUTO_FOCUS_STOP",
                                                "V4L2_CID_AUTO_FOCUS_STATUS",
                                                "V4L2_CID_AUTO_FOCUS_RANGE",
                                                "V4L2_CID_PAN_SPEED",
                                                "V4L2_CID_TILT_SPEED" };
    static const std::string FMT_TYPE[] = {"",
                                            "V4L2_BUF_TYPE_VIDEO_CAPTURE",
                                            "V4L2_BUF_TYPE_VIDEO_OUTPUT",
                                            "V4L2_BUF_TYPE_VIDEO_OVERLAY",
                                            "V4L2_BUF_TYPE_VBI_CAPTURE",
                                            "V4L2_BUF_TYPE_VBI_OUTPUT",
                                            "V4L2_BUF_TYPE_SLICED_VBI_CAPTURE",
                                            "V4L2_BUF_TYPE_SLICED_VBI_OUTPUT",
                                            "V4L2_BUF_TYPE_VIDEO_OUTPUT_OVERLAY",
                                            "V4L2_BUF_TYPE_VIDEO_CAPTURE_MPLANE",
                                            "V4L2_BUF_TYPE_VIDEO_OUTPUT_MPLANE",
                                            "V4L2_BUF_TYPE_SDR_CAPTURE",
                                            "V4L2_BUF_TYPE_SDR_OUTPUT",
                                            "V4L2_BUF_TYPE_META_CAPTURE"};
    static const std::string FMT_FLAGS[] = {"",
                                           "V4L2_FMT_FLAG_COMPRESSED",
                                           "V4L2_FMT_FLAG_EMULATED"};


    bool Camera::checkFile(std::string name) {
        int fd = -1;
        int r;
        struct stat st;
        v4l2_capability cap;
        bool ret = true;

        if (DEBUG) ALOGV("checkFile: %s checking...", name.c_str());

        do {
            if (stat(name.c_str(), &st) == -1) {
                if (DEBUG)
                    ALOGE("checkFile: error: cannot identity the device: %s: %s!", name.c_str(),
                          strerror(errno));
                ret = false;
                break;
            }

            if (!S_ISCHR (st.st_mode)) {
                if (DEBUG) ALOGE("checkFile: error: %s is not a device!", name.c_str());
                ret = false;
                break;
            }

            fd = open(name.c_str(), O_RDWR | O_NONBLOCK);
            if (fd <= 0) {
                if (DEBUG) ALOGE("checkFile: error: %s cannot open: %s!", name.c_str(), strerror(errno));
                ret = false;
                break;
            }

            r = xioctl(fd, VIDIOC_QUERYCAP, &cap);
            if (r == -1 && ((errno == EINVAL) || (errno == ENOTTY))) {
                if (DEBUG) ALOGE("%s is not a V4L2 device!", name.c_str());
                ret = false;
                break;
            }

            if (r == 0) {
                if (DEBUG) ALOGV("Driver: %s\n", cap.driver);
                if (DEBUG) ALOGV("Device: %s\n", cap.card);
                if (DEBUG) ALOGV("Bus: %s\n", cap.bus_info);
                if (DEBUG) ALOGV("Version: %u.%u.%u\n", ((cap.version >> 16) & 0xFF), ((cap.version >> 8) & 0xFF), (cap.version & 0xFF));
                if (DEBUG) ALOGV("Phys Capabilities: %08X\n", cap.capabilities);
                if (DEBUG) ALOGV("Dev Capabilities: %08X\n", cap.device_caps);

                // Check for capabilities
                if (strstr((char *)&cap.bus_info[0], VIDEO_BUS.c_str()) == NULL) {
                    if (DEBUG) ALOGE("checkFile: error: %s is not a USB device!", name.c_str());
                    ret = false;
                }
                if (!(cap.capabilities & V4L2_CAP_VIDEO_CAPTURE)) {
                    if (DEBUG) ALOGE("checkFile: error: %s is not a video capture device!", name.c_str());
                    ret = false;
                }
                if (!(cap.capabilities & V4L2_CAP_STREAMING)) {
                    if (DEBUG) ALOGE("checkFile: error: %s does not support streaming!", name.c_str());
                    ret = false;
                }
            }
        } while (0);

        if (fd >= 0) close(fd);

        return ret;
    }

    std::string Camera::find(void) {
        if (DEBUG) ALOGV("find\n");

        DIR *dir;
        struct dirent *ent;
        std::string tmp;

        if ((dir = opendir(VIDEO_DIR.c_str())) != NULL) {
            if (DEBUG) ALOGV("find: checking in %s folder...\n", VIDEO_DIR.c_str());
            while ((ent = readdir(dir)) != NULL) {
                tmp = VIDEO_DIR + "/" + std::string(ent->d_name);
                if (tmp.find(VIDEO_FILE) != std::string::npos) {
                    /* 'video*' file name found: looking for 'usb' bus */
                    if (checkFile(tmp)) {
                        if (DEBUG) ALOGD("find: %s found!\n", tmp.c_str());
                        closedir(dir);
                        return tmp;
                    }
                }
            }
            if (DEBUG) ALOGE("find: no file found!\n");
            closedir(dir);
            return std::string();
        } else {
            /* could not open directory */
            if (DEBUG) ALOGE("find: error on %s opening(%s)!\n", VIDEO_DIR.c_str(), strerror(errno));
            return std::string();
        }
    }


    Camera::Camera(void) {
        if (DEBUG) ALOGV("constructor\n");

        this->m_fd = -1;
        this->m_num_buffers = 0;
        this->m_width = this->m_height = this->m_bytesperline = 0;
        this->m_frame_sizes.clear();
    }

    void Camera::Init(unsigned int num) {
        std::string found;

        if (DEBUG) ALOGV("Init(%u)\n", num);

        found = find();
        if (found.empty())
            throw AEXCP("Error: no camera found!");

        this->m_fd = open(found.c_str(), O_RDWR | O_NONBLOCK);
        if (this->m_fd <= 0)
            throw AEXCP("error on " + found + " opening (" + strerror(errno) + ")!");

        this->m_num_buffers = num;
        this->m_cameraBuffer = new unsigned char *[this->m_num_buffers];

        this->initInfo();

        this->setFrameSize(320, 240);

        this->initMemoryMap();
    }

    void Camera::Dispose() {
        if (DEBUG) ALOGV("Dispose\n");

        this->clearMemoryMap();
        this->clearInfo();

        delete[] this->m_cameraBuffer;
        this->m_num_buffers = 0;

        if (this->m_fd > 0) {
            ::close(this->m_fd);
            this->m_fd = -1;
        }
    }

    void Camera::initMemoryMap() {
        int r;
        v4l2_requestbuffers req;

        if (DEBUG) ALOGV("initMemoryMap\n");

        memset(&req, 0, sizeof(req));

        req.count = this->m_num_buffers;
        req.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        req.memory = V4L2_MEMORY_MMAP;

        r = xioctl(this->m_fd, VIDIOC_REQBUFS, &req);
        if (r == -1)
            throw AEXCP("error on requesting buffer (" + std::string(strerror(errno)) + ")!");

        v4l2_buffer buf;

        for (int counter = 0; counter < this->m_num_buffers; counter++) {
            memset(&buf, 0, sizeof(buf));
            buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
            buf.memory = V4L2_MEMORY_MMAP;
            buf.index = counter;

            r = xioctl(this->m_fd, VIDIOC_QUERYBUF, &buf);
            if (r == -1)
                throw AEXCP("error on VIDIOC_QUERYBUF (" + std::string(strerror(errno)) + ")!");

            this->m_cameraBuffer[counter] = reinterpret_cast<unsigned char *>(mmap(0, buf.length,
                                                                             PROT_READ | PROT_WRITE,
                                                                             MAP_SHARED,
                                                                             this->m_fd,
                                                                             buf.m.offset));
            if (this->m_cameraBuffer[counter] == MAP_FAILED)
                throw AEXCP("error on initializing memory map (" + std::string(strerror(errno)) + ")!");

            memset(this->m_cameraBuffer[counter], 0, buf.length);
        }
    }

    void Camera::clearMemoryMap() {
        int r;
        int counter;
        v4l2_buffer buf;

        if (DEBUG) ALOGV("clearMemoryMap\n");

        for (counter = 0; counter < this->m_num_buffers; counter++) {
            memset(&buf, 0, sizeof(buf));

            buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
            buf.memory = V4L2_MEMORY_MMAP;
            buf.index = counter;

            r = xioctl(this->m_fd, VIDIOC_QUERYBUF, &buf);
            if (r == -1)
                throw AEXCP("error on VIDIOC_QUERYBUF (" + std::string(strerror(errno)) + ")!");

            if (-1 == munmap(this->m_cameraBuffer[counter], buf.length)) {
                if (DEBUG) ALOGE("clearMemoryMap: error on munmap(%d): %s\n", counter, strerror(errno));
            }
            this->m_cameraBuffer[counter] = NULL;
        }
    }

    bool Camera::convertYUYV2RGB32 (unsigned int index, unsigned char *buf, unsigned int size) {
        const unsigned char *inFrame = this->m_cameraBuffer[index];
        RGB32 *outFrame = (RGB32 *)buf;

        if (DEBUG) ALOGV("convertYUYV2RGB32(%u)\n", index);

        if (size < (this->m_width*this->m_height*4)) {
            if (DEBUG) ALOGE("convertYUYV2RGB32(%d): not enough room for image (%u)\n", index, (this->m_width*this->m_height*4));
            return false;
        }

        for (unsigned int i = 0; i < this->m_height; i++) {
            for (unsigned int j = 0; j < this->m_width; j += 2) {
                int u = inFrame[1];
                int v = inFrame[3];
                int u1 = (((u - 128) << 7) + (u - 128)) >> 6;
                int rg = (((u - 128) << 1) + (u - 128) + ((v - 128) << 2) + ((v - 128) << 1)) >> 3;
                int v1 = (((v - 128) << 1) + (v - 128)) >> 1;

                outFrame->r = CLIP((inFrame[0] + v1));
                outFrame->g = CLIP((inFrame[0] - rg));
                outFrame->b = CLIP((inFrame[0] + u1));
                outFrame->a = 0xFF;
                outFrame++;
                outFrame->r = CLIP((inFrame[2] + v1));
                outFrame->g = CLIP((inFrame[2] - rg));
                outFrame->b = CLIP((inFrame[2] + u1));
                outFrame->a = 0xFF;
                outFrame++;

                inFrame += 4;
            }
        }

        return true;
    }

    void Camera::startCapturing() {
        int r;
        v4l2_buf_type type;
        v4l2_buffer buf;

        if (DEBUG) ALOGV("startCapturing\n");

        for (int i = 0; i < 2; i++) {
            memset(&buf, 0, sizeof(buf));
            buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
            buf.memory = V4L2_MEMORY_MMAP;
            buf.index = i;

            r = xioctl(this->m_fd, VIDIOC_QBUF, &buf);
            if (r == -1)
                throw AEXCP("error on VIDIOC_QBUF (" + std::string(strerror(errno)) + ")!");
        }

        type = V4L2_BUF_TYPE_VIDEO_CAPTURE;

        r = xioctl(this->m_fd, VIDIOC_STREAMON, &type);
        if (r == -1)
            throw AEXCP("error on VIDIOC_STREAMON (" + std::string(strerror(errno)) + ")!");
    }

    void Camera::stopCapturing() {
        int r;
        v4l2_buf_type type;

        if (DEBUG) ALOGV("stopCapturing\n");

        type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        r = xioctl(this->m_fd, VIDIOC_STREAMOFF, &type);
        if (r == -1)
            throw AEXCP("error on VIDIOC_STREAMOFF (" + std::string(strerror(errno)) + ")!");
    }

    bool Camera::captureFrame (ImageFormat fmt, unsigned char *buf, unsigned int size) {
        bool ret = true;
        v4l2_buffer dqBuf;
        int r;

        if (DEBUG) ALOGV("captureFrame(%d)\n", fmt);

        {
            fd_set fds;
            timeval tv;
            int r;

            FD_ZERO (&fds);
            FD_SET (this->m_fd, &fds);

            tv.tv_sec = 2;
            tv.tv_usec = 0;

            r = select(this->m_fd + 1, &fds, 0, 0, &tv);

            if ((r == -1 && errno == EINTR) || r == 0) {
                if (DEBUG) ALOGE("captureFrame(%d): error on select: %s\n", fmt, strerror(errno));
                this->stopCapturing();

                return false;
            }
        }

        memset(&dqBuf, 0, sizeof(dqBuf));

        dqBuf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        dqBuf.memory = V4L2_MEMORY_MMAP;

        r = xioctl(this->m_fd, VIDIOC_DQBUF, &dqBuf);
        if (r == -1) {
            if (DEBUG) ALOGE("captureFrame(%d): error on ioctl(VIDIOC_DQBUF): %s\n", fmt, strerror(errno));

            this->stopCapturing();
            return false;
        }

        if (fmt == IMAGE_RGB) {
            ret = convertYUYV2RGB32(dqBuf.index, buf, size);
        } else {
            if (size < dqBuf.bytesused) {
                if (DEBUG) ALOGE("captureFrame(%d): not enough room for image (%u)\n", fmt, dqBuf.bytesused);
                ret = false;
            } else {
                memcpy(buf, this->m_cameraBuffer[dqBuf.index], dqBuf.bytesused);
            }
        }

        // buf.index wss already set
        r = xioctl(this->m_fd, VIDIOC_QBUF, &dqBuf);
        if (r == -1) {
            if (DEBUG) ALOGE("captureFrame(%d): error on ioctl(VIDIOC_QBUF): %s\n", fmt, strerror(errno));
        }

        return ret;
    }

    bool Camera::getFrameSize (unsigned int index, unsigned int &w, unsigned int &h) {
        if (DEBUG) ALOGV("getFrameSize(%u)\n", index);

        if (index < this->m_frame_sizes.size()) {
            w = this->m_frame_sizes[index].width;
            h = this->m_frame_sizes[index].height;

            return true;
        } else {
            if (DEBUG) ALOGV("getFrameSize(%u): index out of range (0..%u)\n", index, (unsigned int)(this->m_frame_sizes.size()-1));

            return false;
        }
    }

    bool Camera::getFrameSize (unsigned int &w, unsigned int &h) {
        if (DEBUG) ALOGV("getFrameSize()\n");

        w = this->m_width;
        h = this->m_height;

        return true;
    }

    bool Camera::setFrameSize (unsigned int index) {
        if (DEBUG) ALOGV("setFrameSize(%u)\n", index);

        if (index < this->m_frame_sizes.size()) {
            return this->setFrameSize(this->m_frame_sizes[index].width, this->m_frame_sizes[index].height);
        } else {
            if (DEBUG) ALOGV("setFrameSize(%u): index out of range (0..%u)\n", index, (unsigned int)(this->m_frame_sizes.size()-1));

            return false;
        }
    }

    bool Camera::setFrameSize(unsigned int w, unsigned int h) {
        int r;
        v4l2_format fmt;

        if (DEBUG) ALOGV("setVideoFormat(%ux%u)\n", w, h);

        memset(&fmt, 0, sizeof(fmt));

        fmt.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        fmt.fmt.pix.width = w;
        fmt.fmt.pix.height = h;
        fmt.fmt.pix.pixelformat = V4L2_PIX_FMT_YUYV;
        fmt.fmt.pix.field = V4L2_FIELD_INTERLACED;

        r = xioctl(this->m_fd, VIDIOC_S_FMT, &fmt);
        if (r == -1)
            throw AEXCP("error on setting video format (" + std::string(strerror(errno)) + ")!");

        m_width = fmt.fmt.pix.width;
        m_height = fmt.fmt.pix.height;

        return true;
    }

    void Camera::initInfo(void) {
#if 0
    {
        int r;
        struct v4l2_input input;

        if (DEBUG) ALOGV("initInfo\n");

        memset(&input, 0, sizeof(input));
        r = xioctl(this->m_fd, VIDIOC_ENUMINPUT, &input);
        if (r == 0) {
            if (DEBUG) ALOGV("init: input index: %u\n", input.index);
            if (DEBUG) ALOGV("init: input name: %s\n", input.name);
            if (DEBUG) ALOGV("init: input type: %u\n", input.type);
            if (DEBUG) ALOGV("init: input audioset: %u\n", input.audioset);
            if (DEBUG) ALOGV("init: input tuner: %u\n", input.tuner);
            if (DEBUG) ALOGV("init: input status: %08X\n", input.status);
            if (DEBUG) ALOGV("init: input capabilities: %08X\n", input.capabilities);
        }
    }

    {
        struct v4l2_queryctrl queryctrl;
        struct v4l2_querymenu querymenu;
        struct v4l2_control control;

        memset(&queryctrl, 0, sizeof(queryctrl));

        queryctrl.id = V4L2_CTRL_FLAG_NEXT_CTRL;
        while (0 == xioctl(this->m_fd, VIDIOC_QUERYCTRL, &queryctrl)) {
            switch (queryctrl.id & 0x00FF0000) {
                case V4L2_CTRL_CLASS_USER: /* 0x980000 */
                    if (DEBUG)
                        ALOGV("init: input control id: %08X (%s)\n", queryctrl.id,
                              USERCTRL_ID[(queryctrl.id & 0xFF)].c_str());
                    break;
                case V4L2_CTRL_CLASS_MPEG: /* 0x990000 */
                    if (DEBUG) ALOGV("init: input control id: %08X\n", queryctrl.id);
                    break;
                case V4L2_CTRL_CLASS_CAMERA: /* 0x9A0000 */
                    if (DEBUG)
                        ALOGV("init: input control id: %08X (%s)\n", queryctrl.id,
                              CAMERACTRL_ID[(queryctrl.id & 0xFF)].c_str());
                    break;
                case V4L2_CTRL_CLASS_FM_TX: /* 0x9B0000 */
                    if (DEBUG) ALOGV("init: input control id: %08X\n", queryctrl.id);
                    break;
                case V4L2_CTRL_CLASS_FLASH: /* 0x9C0000 */
                    if (DEBUG) ALOGV("init: input control id: %08X\n", queryctrl.id);
                    break;
                case V4L2_CTRL_CLASS_JPEG: /* 0x9D0000 */
                    if (DEBUG) ALOGV("init: input control id: %08X\n", queryctrl.id);
                    break;
                case V4L2_CTRL_CLASS_IMAGE_SOURCE: /* 0x9E0000 */
                    if (DEBUG) ALOGV("init: input control id: %08X\n", queryctrl.id);
                    break;
                case V4L2_CTRL_CLASS_IMAGE_PROC: /* 0x9F0000 */
                    if (DEBUG) ALOGV("init: input control id: %08X\n", queryctrl.id);
                    break;
                case V4L2_CTRL_CLASS_FM_RX: /* 0xA10000 */
                    if (DEBUG) ALOGV("init: input control id: %08X\n", queryctrl.id);
                    break;
                case V4L2_CTRL_CLASS_RF_TUNER: /* 0xA20000 */
                    if (DEBUG) ALOGV("init: input control id: %08X\n", queryctrl.id);
                    break;
                default:
                    if (DEBUG) ALOGV("init: input control id: %08X\n", queryctrl.id);
                    break;
            }
            if (DEBUG)
                ALOGV("init: input control type: %u (%s)\n", queryctrl.type,
                      CTRL_TYPE[queryctrl.type].c_str());
            if (DEBUG) ALOGV("init: input control name: %s\n", queryctrl.name);
            if (DEBUG) ALOGV("init: input control minimum: %d\n", queryctrl.minimum);
            if (DEBUG) ALOGV("init: input control maximum: %d\n", queryctrl.maximum);
            if (DEBUG) ALOGV("init: input control step: %d\n", queryctrl.step);
            if (DEBUG) ALOGV("init: input control default: %d\n", queryctrl.default_value);
            if (DEBUG) ALOGV("init: input control flags: %08X\n", queryctrl.flags);
            if (!(queryctrl.flags & V4L2_CTRL_FLAG_INACTIVE)) {
                if ((queryctrl.type == V4L2_CTRL_TYPE_MENU) ||
                    (queryctrl.type == V4L2_CTRL_TYPE_INTEGER_MENU)) {
                    memset(&querymenu, 0, sizeof(querymenu));
                    querymenu.id = queryctrl.id;

                    for (querymenu.index = queryctrl.minimum;
                         querymenu.index <= queryctrl.maximum;
                         querymenu.index++) {
                        if (0 == xioctl(this->m_fd, VIDIOC_QUERYMENU, &querymenu)) {
                            if (queryctrl.type == V4L2_CTRL_TYPE_MENU) {
                                if (DEBUG)
                                    ALOGV("init: input control \"%s\": %u. %s\n", queryctrl.name,
                                          querymenu.index, querymenu.name);
                            } else {
                                if (DEBUG)
                                    ALOGV("init: input control \"%s\": %u. %lld\n", queryctrl.name,
                                          querymenu.index, querymenu.value);
                            }
                        }
                    }
                }

                memset(&control, 0, sizeof(control));
                control.id = queryctrl.id;
                if (-1 == xioctl(this->m_fd, VIDIOC_G_CTRL, &control)) {
                    if (DEBUG)
                        ALOGE("init: error on getting input control \"%s\" value: %s\n",
                              queryctrl.name, strerror(errno));
                } else {
                    if (DEBUG)
                        ALOGV("init: input control \"%s\" value: %d\n", queryctrl.name,
                              control.value);
                }
            } else {
                if (DEBUG) ALOGV("init: input control \"%s\" is disabled\n", queryctrl.name);
            }

            queryctrl.id |= V4L2_CTRL_FLAG_NEXT_CTRL;
        }
    }

    {
        struct v4l2_query_ext_ctrl query_ext_ctrl;
        struct v4l2_querymenu querymenu;
        struct v4l2_control control;
        struct v4l2_ext_controls extControls;

        memset(&query_ext_ctrl, 0, sizeof(query_ext_ctrl));

        query_ext_ctrl.id = V4L2_CTRL_FLAG_NEXT_CTRL | V4L2_CTRL_FLAG_NEXT_COMPOUND;
        while (0 == xioctl(this->m_fd, VIDIOC_QUERY_EXT_CTRL, &query_ext_ctrl)) {
            switch (query_ext_ctrl.id & 0x00FF0000) {
                case V4L2_CTRL_CLASS_USER: /* 0x980000 */
                    if (DEBUG)
                        ALOGV("init: input control ext id: %08X (%s)\n", query_ext_ctrl.id,
                              USERCTRL_ID[(query_ext_ctrl.id & 0xFF)].c_str());
                    break;
                case V4L2_CTRL_CLASS_MPEG: /* 0x990000 */
                    if (DEBUG) ALOGV("init: input control ext id: %08X\n", query_ext_ctrl.id);
                    break;
                case V4L2_CTRL_CLASS_CAMERA: /* 0x9A0000 */
                    if (DEBUG)
                        ALOGV("init: input control ext id: %08X (%s)\n", query_ext_ctrl.id,
                              CAMERACTRL_ID[(query_ext_ctrl.id & 0xFF)].c_str());
                    break;
                case V4L2_CTRL_CLASS_FM_TX: /* 0x9B0000 */
                    if (DEBUG) ALOGV("init: input control ext id: %08X\n", query_ext_ctrl.id);
                    break;
                case V4L2_CTRL_CLASS_FLASH: /* 0x9C0000 */
                    if (DEBUG) ALOGV("init: input control ext id: %08X\n", query_ext_ctrl.id);
                    break;
                case V4L2_CTRL_CLASS_JPEG: /* 0x9D0000 */
                    if (DEBUG) ALOGV("init: input control ext id: %08X\n", query_ext_ctrl.id);
                    break;
                case V4L2_CTRL_CLASS_IMAGE_SOURCE: /* 0x9E0000 */
                    if (DEBUG) ALOGV("init: input control ext id: %08X\n", query_ext_ctrl.id);
                    break;
                case V4L2_CTRL_CLASS_IMAGE_PROC: /* 0x9F0000 */
                    if (DEBUG) ALOGV("init: input control ext id: %08X\n", query_ext_ctrl.id);
                    break;
                case V4L2_CTRL_CLASS_FM_RX: /* 0xA10000 */
                    if (DEBUG) ALOGV("init: input control ext id: %08X\n", query_ext_ctrl.id);
                    break;
                case V4L2_CTRL_CLASS_RF_TUNER: /* 0xA20000 */
                    if (DEBUG) ALOGV("init: input control ext id: %08X\n", query_ext_ctrl.id);
                    break;
                default:
                    if (DEBUG) ALOGV("init: input control ext id: %08X\n", query_ext_ctrl.id);
                    break;
            }
            if (DEBUG)
                ALOGV("init: input control ext type: %u (%s)\n", query_ext_ctrl.type,
                      CTRL_TYPE[query_ext_ctrl.type].c_str());
            if (DEBUG) ALOGV("init: input control ext name: %s\n", query_ext_ctrl.name);
            if (DEBUG) ALOGV("init: input control ext minimum: %lld\n", query_ext_ctrl.minimum);
            if (DEBUG) ALOGV("init: input control ext maximum: %lld\n", query_ext_ctrl.maximum);
            if (DEBUG) ALOGV("init: input control ext step: %lld\n", query_ext_ctrl.step);
            if (DEBUG)
                ALOGV("init: input control ext default: %lld\n", query_ext_ctrl.default_value);
            if (DEBUG) ALOGV("init: input control ext flags: %08X\n", query_ext_ctrl.flags);
            if (DEBUG) ALOGV("init: input control ext elem size: %d\n", query_ext_ctrl.elem_size);
            if (DEBUG) ALOGV("init: input control ext elem number: %d\n", query_ext_ctrl.elems);
            if (DEBUG) ALOGV("init: input control ext dim number: %d\n", query_ext_ctrl.nr_of_dims);
            for (int counter = 0; counter < query_ext_ctrl.nr_of_dims; counter++) {
                if (DEBUG)
                    ALOGV("init: input control ext dim %d size: %d\n", (counter + 1),
                          query_ext_ctrl.dims[counter]);
            }

            if (!(query_ext_ctrl.flags & V4L2_CTRL_FLAG_INACTIVE)) {
                if ((query_ext_ctrl.type == V4L2_CTRL_TYPE_MENU) ||
                    (query_ext_ctrl.type == V4L2_CTRL_TYPE_INTEGER_MENU)) {
                    memset(&querymenu, 0, sizeof(querymenu));
                    querymenu.id = query_ext_ctrl.id;

                    for (querymenu.index = query_ext_ctrl.minimum;
                         querymenu.index <= query_ext_ctrl.maximum;
                         querymenu.index++) {
                        if (0 == xioctl(this->m_fd, VIDIOC_QUERYMENU, &querymenu)) {
                            if (query_ext_ctrl.type == V4L2_CTRL_TYPE_MENU) {
                                if (DEBUG)
                                    ALOGV("init: input control ext \"%s\": %u. %s\n",
                                          query_ext_ctrl.name, querymenu.index, querymenu.name);
                            } else {
                                if (DEBUG)
                                    ALOGV("init: input control ext \"%s\": %u. %lld\n",
                                          query_ext_ctrl.name, querymenu.index, querymenu.value);
                            }
                        }
                    }
                }

                switch (query_ext_ctrl.id & 0x00FF0000) {
                    case V4L2_CTRL_CLASS_USER: /* 0x980000 */
                        if (DEBUG)
                            ALOGV("init: input control ext \"%s\" class: User Control\n",
                                  query_ext_ctrl.name);
                        memset(&control, 0, sizeof(control));
                        control.id = query_ext_ctrl.id;
                        if (-1 == xioctl(this->m_fd, VIDIOC_G_CTRL, &control)) {
                            if (DEBUG)
                                ALOGE("init: error on getting input control ext \"%s\" value: %s\n",
                                      query_ext_ctrl.name, strerror(errno));
                        } else {
                            if (DEBUG)
                                ALOGV("init: input control ext \"%s\" value: %d\n",
                                      query_ext_ctrl.name, control.value);
                        }
                        break;
                    case V4L2_CTRL_CLASS_MPEG: /* 0x990000 */
                        if (DEBUG)
                            ALOGV("init: input control ext \"%s\" class: Codec Control\n",
                                  query_ext_ctrl.name);
                        break;
                    case V4L2_CTRL_CLASS_CAMERA: /* 0x9A0000 */
                        if (DEBUG)
                            ALOGV("init: input control ext \"%s\" class: Camera Control\n",
                                  query_ext_ctrl.name);
                        memset(&control, 0, sizeof(control));
                        control.id = query_ext_ctrl.id;
                        if (-1 == xioctl(this->m_fd, VIDIOC_G_CTRL, &control)) {
                            if (DEBUG)
                                ALOGE("init: error on getting input control ext \"%s\" value: %s\n",
                                      query_ext_ctrl.name, strerror(errno));
                        } else {
                            if (DEBUG)
                                ALOGV("init: input control ext \"%s\" value: %d\n",
                                      query_ext_ctrl.name, control.value);
                        }
                        break;
                    case V4L2_CTRL_CLASS_FM_TX: /* 0x9B0000 */
                        if (DEBUG)
                            ALOGV("init: input control ext \"%s\" class: FM Transmitter Control\n",
                                  query_ext_ctrl.name);
                        break;
                    case V4L2_CTRL_CLASS_FLASH: /* 0x9C0000 */
                        if (DEBUG)
                            ALOGV("init: input control ext \"%s\" class: Flash Control Control\n",
                                  query_ext_ctrl.name);
                        break;
                    case V4L2_CTRL_CLASS_JPEG: /* 0x9D0000 */
                        if (DEBUG)
                            ALOGV("init: input control ext \"%s\" class: JPEG Control\n",
                                  query_ext_ctrl.name);
                        break;
                    case V4L2_CTRL_CLASS_IMAGE_SOURCE: /* 0x9E0000 */
                        if (DEBUG)
                            ALOGV("init: input control ext \"%s\" class: Image Source Control\n",
                                  query_ext_ctrl.name);
                        break;
                    case V4L2_CTRL_CLASS_IMAGE_PROC: /* 0x9F0000 */
                        if (DEBUG)
                            ALOGV("init: input control ext \"%s\" class: Image Process Control\n",
                                  query_ext_ctrl.name);
                        break;
                    case V4L2_CTRL_CLASS_FM_RX: /* 0xA10000 */
                        if (DEBUG)
                            ALOGV("init: input control ext \"%s\" class: FM Receiver Control\n",
                                  query_ext_ctrl.name);
                        break;
                    case V4L2_CTRL_CLASS_RF_TUNER: /* 0xA20000 */
                        if (DEBUG)
                            ALOGV("init: input control ext \"%s\" class: RF Tuner Control\n",
                                  query_ext_ctrl.name);
                        break;
                    default:
                        if (DEBUG)
                            ALOGV("init: input control ext \"%s\" unexpected class: %08X\n",
                                  query_ext_ctrl.name, query_ext_ctrl.id);
                        break;
                }
            } else {
                if (DEBUG)
                    ALOGV("init: input control ext \"%s\" is disabled\n", query_ext_ctrl.name);
            }

            query_ext_ctrl.id |= V4L2_CTRL_FLAG_NEXT_CTRL | V4L2_CTRL_FLAG_NEXT_COMPOUND;
        }
    }
#endif

    {
        v4l2_fmtdesc fmt;
        int r;

        memset(&fmt, 0, sizeof(fmt));

        for (unsigned int type = 1; type <= 13; type++) {
            do {
                fmt.type = type;

                r = xioctl(this->m_fd, VIDIOC_ENUM_FMT, &fmt);
                if (r == 0) {
#if 0
                    if (DEBUG)
                        ALOGV("init: format %u type: %u (%s)\n", fmt.index, fmt.type,
                              FMT_TYPE[fmt.type].c_str());
                    if (DEBUG)
                        ALOGV("init: format %u flags: %u (%s)\n", fmt.index, fmt.flags,
                              FMT_FLAGS[fmt.flags].c_str());
                    if (DEBUG)
                        ALOGV("init: format %u description: %s\n", fmt.index, fmt.description);
                    if (DEBUG)
                        ALOGV("init: format %u flags: %08X (%c%c%c%c)\n", fmt.index,
                              fmt.pixelformat,
                              (unsigned char) (fmt.pixelformat & 0x000000FF),
                              (unsigned char) ((fmt.pixelformat >> 8) & 0x000000FF),
                              (unsigned char) ((fmt.pixelformat >> 16) & 0x000000FF),
                              (unsigned char) ((fmt.pixelformat >> 24) & 0x000000FF));
#endif
                    {
                        int r;
                        struct v4l2_frmsizeenum frmsize;
                        unsigned int width, height;

                        memset(&frmsize, 0, sizeof(frmsize));

                        do {
                            frmsize.pixel_format = fmt.pixelformat;

                            r = xioctl(this->m_fd, VIDIOC_ENUM_FRAMESIZES, &frmsize);
                            if (r == 0) {
                                if (frmsize.type == V4L2_FRMSIZE_TYPE_DISCRETE) {
                                    FrameSize toAdd = { frmsize.pixel_format, frmsize.discrete.width, frmsize.discrete.height };
                                    this->m_frame_sizes.push_back(toAdd);

                                    if (DEBUG)
                                        ALOGV("init: format %u (%c%c%c%c) framesize %u. %ux%u\n",
                                              fmt.index,
                                              (unsigned char) (fmt.pixelformat & 0x000000FF),
                                              (unsigned char) ((fmt.pixelformat >> 8) & 0x000000FF),
                                              (unsigned char) ((fmt.pixelformat >> 16) & 0x000000FF),
                                              (unsigned char) ((fmt.pixelformat >> 24) & 0x000000FF),
                                              frmsize.index, frmsize.discrete.width,
                                              frmsize.discrete.height);
                                } else if (frmsize.type == V4L2_FRMSIZE_TYPE_STEPWISE) {
                                    if (DEBUG)
                                        ALOGV("init: format %u (%c%c%c%c) framesize %u. width [%u..%u] (%u)\n",
                                              fmt.index,
                                              (unsigned char) (fmt.pixelformat & 0x000000FF),
                                              (unsigned char) ((fmt.pixelformat >> 8) & 0x000000FF),
                                              (unsigned char) ((fmt.pixelformat >> 16) & 0x000000FF),
                                              (unsigned char) ((fmt.pixelformat >> 24) & 0x000000FF),
                                              frmsize.index, frmsize.stepwise.min_width,
                                              frmsize.stepwise.max_width,
                                              frmsize.stepwise.step_width);
                                    if (DEBUG)
                                        ALOGV("init: format %u (%c%c%c%c) framesize %u. height [%u..%u] (%u)\n",
                                              fmt.index,
                                              (unsigned char) (fmt.pixelformat & 0x000000FF),
                                              (unsigned char) ((fmt.pixelformat >> 8) & 0x000000FF),
                                              (unsigned char) ((fmt.pixelformat >> 16) & 0x000000FF),
                                              (unsigned char) ((fmt.pixelformat >> 24) & 0x000000FF),
                                              frmsize.index, frmsize.stepwise.min_height,
                                              frmsize.stepwise.max_height,
                                              frmsize.stepwise.step_height);
                                } else {
                                    if (DEBUG)
                                        ALOGV("init: format %u (%c%c%c%c) framesize %u. width [%u..%u] (%u)\n",
                                              fmt.index,
                                              (unsigned char) (fmt.pixelformat & 0x000000FF),
                                              (unsigned char) ((fmt.pixelformat >> 8) & 0x000000FF),
                                              (unsigned char) ((fmt.pixelformat >> 16) & 0x000000FF),
                                              (unsigned char) ((fmt.pixelformat >> 24) & 0x000000FF),
                                              frmsize.index, frmsize.stepwise.min_width,
                                              frmsize.stepwise.max_width,
                                              1);
                                    if (DEBUG)
                                        ALOGV("init: format %u (%c%c%c%c) framesize %u. height [%u..%u] (%u)\n",
                                              fmt.index,
                                              (unsigned char) (fmt.pixelformat & 0x000000FF),
                                              (unsigned char) ((fmt.pixelformat >> 8) & 0x000000FF),
                                              (unsigned char) ((fmt.pixelformat >> 16) & 0x000000FF),
                                              (unsigned char) ((fmt.pixelformat >> 24) & 0x000000FF),
                                              frmsize.index, frmsize.stepwise.min_height,
                                              frmsize.stepwise.max_height,
                                              1);
                                }
                            } else if (r == -1) {
                                break;
                            }

                            frmsize.index++;
                        } while (r == 0);
                    }
                } else if (r == -1) {
                    break;
                }

                fmt.index++;
            } while (r == 0);
        } /* for */
    }

}

    void Camera::clearInfo(void) {
        if (DEBUG) ALOGV("clearInfo\n");

        this->m_frame_sizes.clear();
    }

}