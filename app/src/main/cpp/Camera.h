/*
 * camera.h
 *
 *  Created on: 26 giu 2019
 *      Author: andre
 */

#ifndef CAMERA_H_
#define CAMERA_H_

#include <vector>
#include <string>

namespace VisionAR {

    typedef struct {
        unsigned int pixelformat;
        unsigned int width;
        unsigned int height;
    } FrameSize;

    class Camera
    {
        int m_fd;

        unsigned int m_num_buffers; // number of mmap buffers
        unsigned char **m_cameraBuffer;

        unsigned int m_width;
        unsigned int m_height;
        unsigned int m_bytesperline;
        std::vector<FrameSize> m_frame_sizes;

        private:
            static bool checkFile(std::string name);
            static std::string find(void);  /* look for video device file and return descriptor */

            void initInfo(void);
            void clearInfo(void);

            bool convertYUYV2RGB32 (unsigned int index, unsigned char *buf, unsigned int size);

        public:
            enum ImageFormat { IMAGE_RGB, IMAGE_YUYV };

            Camera (void);
            ~Camera (void);

            // Initialize camera memory (num is number of buffers)
            void Init (unsigned int num = 2);
            void Dispose (void);

            void initMemoryMap (void);
            void clearMemoryMap (void);

            void startCapturing (void);
            void stopCapturing (void);

            bool captureFrame (ImageFormat fmt, unsigned char *buf, unsigned int size);

            // get index-th frame size (false when there is no info)
            bool getFrameSize (unsigned int index, unsigned int &w, unsigned int &h);
            // get current frame size (false when there is no info)
            bool getFrameSize (unsigned int &w, unsigned int &h);

            // set camera to index-th frame size (false in case of failure)
            bool setFrameSize (unsigned int index);
            // set camera to given frame size (false in case of failure)
            bool setFrameSize (unsigned int w, unsigned int h);
    };

} // End namespace

#endif /* CAMERA_H_ */
