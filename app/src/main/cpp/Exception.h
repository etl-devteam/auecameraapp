#ifndef CAMERALIB_EXCEPTION_H
#define CAMERALIB_EXCEPTION_H

#include <exception>
#include <string>

namespace VisionAR {

class Exception {
    std::string m_msg;

public:
    Exception (const std::string &file, const int &line, const std::string &msg);

    virtual const char *what () const noexcept;
};

} // End namespace

#endif //CAMERALIB_EXCEPTION_H
