#include <jni.h>
#include <string>
#include <errno.h>

#include "ALog.h"
#include "Camera.h"


using namespace VisionAR;

static const char *TAG = "LIBCAMERA";
static const int DEBUG = true;

static void throwJavaException(JNIEnv *env, const char *msg)
{
    if (DEBUG) ALOGV("throwJavaException: %s\n", msg);

    // You can put your own exception here
    jclass c = env->FindClass("java/lang/Exception");
    if (NULL == c)
    {
        //B plan: null pointer ...
        c = env->FindClass("java/lang/NullPointerException");
    }

    env->ThrowNew(c, msg);
}

extern "C" JNIEXPORT jlong JNICALL Java_it_univet_visionar_libcamera_VisionARCamera_nativeCreate(JNIEnv* env, jobject /* this */) {
    if (DEBUG) ALOGV("nativeCreate\n");

    return reinterpret_cast<long>(new Camera());
}

extern "C" JNIEXPORT void JNICALL Java_it_univet_visionar_libcamera_VisionARCamera_nativeInit(JNIEnv* env, jobject /* this */, jlong id) {
    if (DEBUG) ALOGV("nativeInit\n");

    Camera *camera = reinterpret_cast<Camera *>(id);
    try {
        camera->Init();
    }
    catch (Exception *e) {
        throwJavaException (env, e->what());
    }
}

extern "C" JNIEXPORT jboolean JNICALL Java_it_univet_visionar_libcamera_VisionARCamera_nativeReadFrame(JNIEnv* env, jobject /* this */, jlong id, jbyteArray array) {
    if (DEBUG) ALOGV("nativeReadFrame\n");

    bool ret = false;
    Camera *camera = reinterpret_cast<Camera *>(id);
    /* Get the size of the array */
    jsize len = env->GetArrayLength(array);
    /* Get array's body */
    jbyte *body = env->GetByteArrayElements(array, 0);

    try {
        camera->startCapturing();
        ret = camera->captureFrame(Camera::IMAGE_RGB, (unsigned char *)body, len);
        camera->stopCapturing();

        /* Release the body and commit changes */
        env->ReleaseByteArrayElements(array, body, JNI_COMMIT);
    }
    catch (Exception *e) {
        throwJavaException (env, e->what());
    }

    return (jboolean)ret;
}

extern "C" JNIEXPORT jintArray JNICALL Java_it_univet_visionar_libcamera_VisionARCamera_nativeGetFrameSize (JNIEnv* env, jobject /* this */, jlong id) {
    if (DEBUG) ALOGV("nativeGetFrameSize\n");

    unsigned int val[2] = { 0, 0 };
    jintArray ret = env->NewIntArray(2);

    Camera *camera = reinterpret_cast<Camera *>(id);
    try {
        if (!camera->getFrameSize(val[0], val[1])) {
            if (DEBUG) ALOGE("nativeGetFrameSize: error on getFrameSize\n");
            val[0] = val[1] = 0;
        }
    }
    catch (Exception *e) {
        throwJavaException (env, e->what());
    }

    env->SetIntArrayRegion(ret, 0, 2, (jint *)&val[0]);
    return ret;
}

extern "C" JNIEXPORT jintArray JNICALL Java_it_univet_visionar_libcamera_VisionARCamera_nativeGetFrameSizeIndex (JNIEnv* env, jobject /* this */, jlong id, jint index) {
    if (DEBUG) ALOGV("nativeGetFrameSize(%d)\n", index);

    unsigned int val[2] = { 0, 0 };
    jintArray ret = env->NewIntArray(2);

    Camera *camera = reinterpret_cast<Camera *>(id);
    try {
        if (!camera->getFrameSize((unsigned int)index, val[0], val[1])) {
            if (DEBUG) ALOGE("nativeGetFrameSize: error on getFrameSize\n");
            val[0] = val[1] = 0;
        }
    }
    catch (Exception *e) {
        throwJavaException (env, e->what());
    }

    env->SetIntArrayRegion(ret, 0, 2, (jint *)&val[0]);
    return ret;
}

extern "C" JNIEXPORT jboolean JNICALL Java_it_univet_visionar_libcamera_VisionARCamera_nativeSetFrameSize (JNIEnv* env, jobject /* this */, jlong id, jint w, jint h) {
    if (DEBUG) ALOGV("nativeSetFrameSize(%dx%d)\n", w, h);

    bool ret = false;

    Camera *camera = reinterpret_cast<Camera *>(id);
    try {
         ret = camera->setFrameSize(w, h);
    }
    catch (Exception *e) {
        throwJavaException (env, e->what());
    }

    return (jboolean)ret;
}

extern "C" JNIEXPORT jboolean JNICALL Java_it_univet_visionar_libcamera_VisionARCamera_nativeSetFrameSizeIndex (JNIEnv* env, jobject /* this */, jlong id, jint index) {
    if (DEBUG) ALOGV("nativeSetFrameSize(%d)\n", index);

    bool ret = false;

    Camera *camera = reinterpret_cast<Camera *>(id);
    try {
        ret = camera->setFrameSize((unsigned int)index);
    }
    catch (Exception *e) {
        throwJavaException (env, e->what());
    }

    return (jboolean)ret;
}

/*
extern "C" JNIEXPORT void nativeDestroy(JNIEnv *env, jobject thiz,
                          ID_TYPE id_camera) {

    ENTER();
    setField_long(env, thiz, "mNativePtr", 0);
    UVCCamera *camera = reinterpret_cast<UVCCamera *>(id_camera);
    if (LIKELY(camera)) {
        SAFE_DELETE(camera);
    }
    EXIT();
}
 */

