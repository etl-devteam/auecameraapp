#ifndef ALOG_H_
#define ALOG_H_

#include <android/log.h>

#include "Exception.h"


// Logging
#define ALOGV(FMT, ...) __android_log_print(ANDROID_LOG_VERBOSE, TAG, "[%s:%d]: " FMT, basename(__FILE__), __LINE__, ## __VA_ARGS__)
#define ALOGD(FMT, ...) __android_log_print(ANDROID_LOG_DEBUG, TAG, "[%s:%d]: " FMT, basename(__FILE__), __LINE__, ## __VA_ARGS__)
#define ALOGI(FMT, ...) __android_log_print(ANDROID_LOG_INFO, TAG, "[%s:%d]: " FMT, basename(__FILE__), __LINE__, ## __VA_ARGS__)
#define ALOGW(FMT, ...) __android_log_print(ANDROID_LOG_WARN, TAG, "[%s:%d]: " FMT, basename(__FILE__), __LINE__, ## __VA_ARGS__)
#define ALOGE(FMT, ...) __android_log_print(ANDROID_LOG_ERROR, TAG, "[%s:%d]: " FMT, basename(__FILE__), __LINE__, ## __VA_ARGS__)
#define ALOGF(FMT, ...) __android_log_print(ANDROID_LOG_FATAL, TAG, "[%s:%d]: " FMT, basename(__FILE__), __LINE__, ## __VA_ARGS__)


//Exception
#define AEXCP(...) new Exception (basename(__FILE__), __LINE__, ## __VA_ARGS__)

#endif /* ALOG_H_ */
