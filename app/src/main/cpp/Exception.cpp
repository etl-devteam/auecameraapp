#include "Exception.h"

namespace VisionAR {

    Exception::Exception (const std::string &file, const int &line, const std::string &msg)
    {
        m_msg = msg + " ("  + file + ":" + std::to_string(line) + ")";
    }

    const char *Exception::what () const noexcept
    {
        return this->m_msg.c_str ();
    }

}