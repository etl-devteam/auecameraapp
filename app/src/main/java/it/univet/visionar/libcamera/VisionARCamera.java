package it.univet.visionar.libcamera;

import android.graphics.Bitmap;
import android.util.Log;

import java.io.FileOutputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class VisionARCamera {

    static private String TAG = "VisionARCamera";

    // Used to load the 'camera-lib' library on application startup.
    static {
        System.loadLibrary("camera");
    }

    private long mCamera;


    public VisionARCamera() {
        int counter = 0;
        int frmSize[];

        mCamera = nativeCreate();
        Log.d(TAG, "Camera was created");

        try {
            nativeInit(mCamera);
            Log.d(TAG, "Camera was initialized");

            do {
                frmSize = nativeGetFrameSizeIndex(mCamera, counter);
                if (frmSize[0] != 0) {
                    Log.d(TAG, "Frame size " + counter + ". " + frmSize[0] + "x" + frmSize[1]);
                }

                counter++;
            } while (frmSize[0] != 0);
//            nativeSetFrameSizeIndex(mCamera, 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Bitmap readFrame() {
        int frmSize[];
        byte bytes[];

        frmSize = nativeGetFrameSize(mCamera);
        bytes =  new byte[frmSize[0]*frmSize[1]*4];

        try {
            nativeReadFrame(mCamera, bytes);
            Log.d(TAG, "Frame was read");

            Bitmap bmp = Bitmap.createBitmap(frmSize[0], frmSize[1], Bitmap.Config.ARGB_8888);
            ByteBuffer buffer = ByteBuffer.wrap(bytes);
            bmp.copyPixelsFromBuffer(buffer);

            return bmp;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void saveImageAsPNG(Bitmap bmp, String filename) {
        Log.d(TAG, "saveImageAsPNG(" + filename + ")");

        try (FileOutputStream out = new FileOutputStream(filename)) {
            bmp.compress(Bitmap.CompressFormat.PNG, 100, out); // bmp is your Bitmap instance
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void saveImageAsJPEG(Bitmap bmp, String filename) {
        Log.d(TAG, "saveImageAsJPEG(" + filename + ")");

        try (FileOutputStream out = new FileOutputStream(filename)) {
            bmp.compress(Bitmap.CompressFormat.JPEG, 100, out); // bmp is your Bitmap instance
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void saveImageAsBMP(Bitmap bmp, String filename) {
        Log.d(TAG, "saveImageAsBMP(" + filename + ")");

        try (FileOutputStream out = new FileOutputStream(filename)) {
            //image size
            int width = bmp.getWidth();
            int height = bmp.getHeight();

            //an array to receive the pixels from the source image
            int[] pixels = new int[width * height];

            //the number of bytes used in the file to store raw image data (excluding file headers)
            int imageSize = 4 * width * height;
            //file headers size
            int imageDataOffset = 0x36;

            //final size of the file
            int fileSize = imageSize + imageDataOffset;

            //Android Bitmap Image Data
            bmp.getPixels(pixels, 0, width, 0, 0, width, height);

            //ByteArrayOutputStream baos = new ByteArrayOutputStream(fileSize);
            ByteBuffer buffer = ByteBuffer.allocate(fileSize);
            buffer.order(ByteOrder.LITTLE_ENDIAN);

            /** BITMAP FILE HEADER Write Start */
            buffer.put((byte)0x42); // B
            buffer.put((byte)0x4D); // M

            //size
            buffer.putInt(fileSize);

            //reserved
            buffer.putShort((short)0);
            buffer.putShort((short)0);

            //image data start offset
            buffer.putInt(imageDataOffset);
            /** BITMAP FILE HEADER Write End */


            /** BITMAP INFO HEADER Write Start */
            //size
            buffer.putInt(0x28);

            //width, height
            //if we add 3 dummy bytes per row : it means we add a pixel (and the image width is modified.
            buffer.putInt(width);
            buffer.putInt(height);

            //planes
            buffer.putShort((short)1);

            //bit count
            buffer.putShort((short)32);

            //bit compression
            buffer.putInt(0);

            //image data size
            buffer.putInt(imageSize);

            //horizontal resolution in pixels per meter
            buffer.putInt(0);

            //vertical resolution in pixels per meter (unreliable)
            buffer.putInt(0);

            buffer.putInt(0);

            buffer.putInt(0);
            /** BITMAP INFO HEADER Write End */

            /** BITMAP PAYLOAD Write Start */
            int row = height;
            int col;
            while( row > 0 ){
                for(col = 0; col < width; col++ ){
                    buffer.putInt(pixels[(row-1)*width + col]);
//                    buffer.put((byte)((pixels[(row-1)*width + col]      ) & 0x000000FF));
//                    buffer.put((byte)((pixels[(row-1)*width + col] >>  8) & 0x000000FF));
//                    buffer.put((byte)((pixels[(row-1)*width + col] >> 16) & 0x000000FF));
//                    buffer.put((byte)((pixels[(row-1)*width + col] >> 24) & 0x000000FF));
                }
                row--;
            }
            /** BITMAP PAYLOAD Write END */

            out.write(buffer.array());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * A native method that is implemented by the 'camera-lib' native library,
     * which is packaged with this application.
     */
    public native long nativeCreate();
    public native void nativeInit (long camId);
    public native void nativeDispose ();

    public native boolean nativeReadFrame (long camId, byte [] array);
    public native int[] nativeGetFrameSize (long camId);
    public native int[] nativeGetFrameSizeIndex (long camId, int index);
    public native boolean nativeSetFrameSize (long camId, int w, int h);
    public native boolean nativeSetFrameSizeIndex (long camId, int index);
}
