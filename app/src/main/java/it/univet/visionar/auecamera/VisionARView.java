package it.univet.visionar.auecamera;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.FrameLayout;

import it.univet.visionar.libvarp.VisionARCanvas;

public class VisionARView extends FrameLayout {

    static private String TAG = "VisionARView";

        public VisionARView(Context context, AttributeSet attrs)  {
        super(context, attrs);
    }

    @Override
    public void dispatchDraw(Canvas canvas) {
        Log.v(TAG, "dispatchDraw");

        Bitmap bitmap = Bitmap.createBitmap(this.getWidth(), this.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas newCanvas = new Canvas(bitmap);

        super.dispatchDraw(newCanvas);

        VisionARCanvas myCanvas = new VisionARCanvas(getResources().getString(R.string.ip_address), getResources().getInteger(R.integer.req_port));

        myCanvas.drawBitmap(bitmap);
        myCanvas.draw();
    }
}
