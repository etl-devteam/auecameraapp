package it.univet.visionar.auecamera;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.FileOutputStream;
import java.util.Date;

import it.univet.visionar.libcamera.VisionARCamera;

import it.univet.visionar.libvarp.VisionARClientNotificationTask;
import it.univet.visionar.libvarp.VisionARClientTask;
import it.univet.visionar.libvarp.VisionARClientTaskParams;
import it.univet.visionar.libvarp.VisionAROutputSetFBMessage;

public class MainActivity extends AppCompatActivity {

    static private String TAG = "AUECAMERA";

    private VisionARCamera mCamera;
    private Handler mHandler;
    private VisionARClientNotificationTask mNtfTask;

    private ViewGroup mLayoutView;
    private TextView mTextView;
    private ImageView mImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.d(TAG, "STARTED!");

        initView();

        mCamera = new VisionARCamera();

        mHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                if (msg.what == 2) {
                    Bitmap bmp = mCamera.readFrame();
                    Log.d(TAG, "Bitmap(" + bmp.getWidth() + "x" + bmp.getHeight() + ") was captured!");

                    mImageView.setImageBitmap(bmp);
                    mTextView.setVisibility(View.INVISIBLE);
                    mImageView.setVisibility(View.VISIBLE);
                    mLayoutView.invalidate();

                    mCamera.saveImageAsPNG(bmp, "/data/local/tmp/test.png");
                    mCamera.saveImageAsJPEG(bmp, "/data/local/tmp/test.jpg");
                    mCamera.saveImageAsBMP(bmp, "/data/local/tmp/test.bmp");
                }
            }
        };

        mNtfTask = new VisionARClientNotificationTask(getResources().getString(R.string.ip_address), getResources().getInteger(R.integer.evt_port)) {
            @Override
            protected void update(int code, int value, Date date) {
                if (value == 1) {
                    Message message = Message.obtain(mHandler, code);
                    message.sendToTarget();
                }
            }
        };

        Thread mNtfThread = new Thread(mNtfTask);
        mNtfThread.start();
    }

    private void initView() {
        setContentView(R.layout.activity_main);

        mLayoutView = findViewById(R.id.layout);
        mTextView = findViewById(R.id.textView);
        mImageView = findViewById(R.id.imageView);

        mTextView.setText("Press enter button...");
        mTextView.setVisibility(View.VISIBLE);

        mImageView.setVisibility(View.INVISIBLE);
        mImageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);

        mLayoutView.invalidate();
    }
}
